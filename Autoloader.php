<?php

//define('ROOTPATH', dirname(__FILE__));

spl_autoload_register(function ($class_name) {

    $all_dir = scandir(ROOTPATH);

    foreach ($all_dir as $k => $v) {

        if (file_exists(ROOTPATH . '/' . $v . '/' . $class_name . '.php')) {

            require_once(ROOTPATH . '/' . $v . '/' . $class_name . '.php');

        }
    }
});




