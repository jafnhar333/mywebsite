<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.02.2019
 * Time: 7:33
 */

class Mgr_educations
{
    private $educations;

    /**
     * Mgr_educations constructor.
     */
    public function __construct()
    {
        $this->setEducations($this->getEducations());
    }

    public function getData()//todo
    {


    }

    public function createEducation()//todo
    {
        $date = date("Y-m-d H:i:s");
        $education = array(
            'name' => 'name',
            'date' => $date,
            'profession' => 'profession',
        );
        $pdo = new This_PDO();
        $pdo->insert($education, 'educations');
    }

    /**
     * @return mixed
     */
    public function getEducations()
    {
        $pdo = new This_PDO();
        $educations = $pdo->select_all('educations', 'Education');

        if (empty($educations)) {
            $this->createEducation();
            $educations = $pdo->select_all('educations', 'Education');
        }
        $this->setEducations($educations);

        return $this->educations;
    }

    /**
     * @param array $educations
     */
    public function setEducations(array $educations)
    {
        $this->educations = $educations;
    }

}