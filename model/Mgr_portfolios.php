<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.02.2019
 * Time: 7:32
 */

class Mgr_portfolios
{

    private $portfolios;

    /**
     * Mgr_portfolios constructor.
     */
    public function __construct()
    {
        $this->setPortfolios($this->getPortfolios());
    }

    public function getData()//todo
    {
        $portfolios = '';
        foreach ($this->portfolios as $portfolio) {
            $portfolios = $portfolios . $portfolio->getPicture() . '&nbsp';
            $portfolios = $portfolios . $portfolio->getName() . '&nbsp';
            $portfolios = $portfolios . $portfolio->getText() . '&nbsp';
            $portfolios = $portfolios . $portfolio->getLink() . '&nbsp' . '</br>';
        }
        return $portfolios;
    }

    public function createPortfolio()//todo
    {
        $portfolio = array(
            'picture' => 'picture',
            'name' => 'name',
            'text' => 'text',
            'link' => 'link'
        );
        $pdo = new This_PDO();
        $pdo->insert($portfolio, 'portfolios');
    }

    /**
     * @return mixed
     */
    public function getPortfolios()
    {
        $pdo = new This_PDO();
        $portfolios = $pdo->select_all('portfolios', 'Portfolio');

        if (empty($portfolios)) {
            $this->createPortfolio();
            $portfolios = $pdo->select_all('portfolios', 'Portfolio');
        }
        $this->setPortfolios($portfolios);

        return $this->portfolios;
    }

    /**
     * @param mixed $portfolios
     */
    public function setPortfolios(array $portfolios)
    {
        $this->portfolios = $portfolios;
    }

}