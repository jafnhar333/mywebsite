<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.02.2019
 * Time: 7:29
 */

class Mgr_social_networks
{
    private $social_networks;

    /**
     * Mgr_social_networks constructor.
     */
    public function __construct()
    {
        $this->setSocialNetworks($this->getSocialNetworks());
    }

    public function getData()//todo
    {
        $social_networks = '';
        foreach ($this->getSocialNetworks() as $social_network) {
            $social_networks = $social_networks . $social_network->getPicture() . '&nbsp';
            $social_networks = $social_networks . $social_network->getName() . '&nbsp';
            $social_networks = $social_networks . $social_network->getLink() . '&nbsp' . '</br>';
        }
        return $social_networks;
    }

    public function createSocialNetwork()//todo
    {
        $social_network = array(
            'picture' => 'ссылка на картинку',
            'name' => 'название соцсети',
            'link' => 'ссылка на сайт соцсети'
        );
        $pdo = new This_PDO();
        $pdo->insert($social_network, 'persons');
    }

    /**
     * @return mixed
     */
    public function getSocialNetworks()
    {
        $pdo = new This_PDO();
        $social_networks = $pdo->select_all('social_networks', 'Social_network');

        if (empty($social_networks)) {
            $this->createSocialNetwork();
            $social_networks = $pdo->select_all('social_networks', 'Social_network');
        }
        $this->setSocialNetworks($social_networks);

        return $this->social_networks;
    }

    /**
     * @param array $social_networks
     */
    public function setSocialNetworks(array $social_networks)
    {
        $this->social_networks = $social_networks;
    }

}