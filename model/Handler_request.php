<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.04.2018
 * Time: 14:26
 */

class Handler_request
{
    private $post;
    private $get;
    private $request;

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post = $_POST;
    }

    /**
     * @return mixed
     */
    public function getGet()
    {
        return $this->get = $_GET;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request = $_REQUEST;
    }

}