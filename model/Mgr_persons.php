<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21.02.2019
 * Time: 16:18
 */

class Mgr_persons
{
    private $persons;

    /**
     * Mgr_persons constructor.
     */
    public function __construct()
    {
        $this->setPersons($this->getPersons());
    }

    public function getData()//todo
    {


    }

    public function createPersona()//todo
    {
        $persona = array(
            'login' => 'login',
            'pass' => 'pass',
            'greeting' => 'greeting',
            'surname' => 'surname',
            'name' => 'name',
            'patronymic' => 'patronymic',
            'address' => 'address',
            'phones' => 'phones',
            'fax' => 'fax',
            'email' => 'email',
            'web_site' => 'web_site',
            'languages' => 'languages',
            'profession' => 'profession',
            'skills' => 'skills',
            'field_of_activity' => 'field_of_activity',
            'photo' => 'photo',
            'driver_license' => 'driver_license'
        );
        $pdo = new This_PDO();
        $pdo->insert($persona, 'persons');
    }

    /**
     * @return array
     */
    public function getPersons()
    {
        $pdo = new This_PDO();
        $persons = $pdo->select_all('persons', 'Persona');

        if (empty($persons)) {
            $this->createPersona();
            $persons = $pdo->select_all('persons', 'Persona');
        }
        $this->setPersons($persons);

        return $this->persons;
    }

    /**
     * @param array $persons
     */
    public function setPersons(array $persons)
    {
        $this->persons = $persons;
    }

}