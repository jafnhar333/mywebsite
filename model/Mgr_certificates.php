<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.02.2019
 * Time: 7:37
 */

class Mgr_certificates
{

    private $certificates;

    /**
     * Mgr_certificates constructor.
     */
    public function __construct()
    {
        $this->setCertificates($this->getCertificates());
    }

    public function getData()//todo
    {


    }

    public function createCertificate()//todo
    {
        $date =  date("Y-m-d H:i:s");
        $certificate = array(
            'name' => 'name',
            'date' => $date,
            'profession' => 'profession',
            'name' => 'name',
            'link' => 'link'
        );
        $pdo = new This_PDO();
        $pdo->insert($certificate, 'certificates');
    }

    /**
     * @return array
     */
    public function getCertificates()
    {
        $pdo = new This_PDO();
        $certificates = $pdo->select_all('certificates', 'Certificate');

        if (empty($certificates)) {
            $this->createCertificate();
            $certificates = $pdo->select_all('certificates', 'Certificate');
        }
        $this->setCertificates($certificates);

        return $this->certificates;
    }

    /**
     * @param array $certificates
     */
    public function setCertificates(array $certificates)
    {
        $this->certificates = $certificates;
    }

}