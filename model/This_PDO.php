<?php


class This_PDO
{
    private $host_name;
    private $db_name;
    private $db_pass;

    /**
     * @return PDO
     */
    public function connection()
    {
        $this->setHostName('127.0.0.1:3306');
        $this->setDbName('a906120w_db');
        $this->setDbPass('123');

        try {
            $dbh = new PDO('mysql:host=' . $this->getHostName() . ';dbname=' . $this->getDbName() . '', $this->getDbName(), $this->getDbPass());
            //$dbh = new PDO('mysql:host=' . $this->getHostName() . ';dbname=' . $this->getDbName() . '', $this->getDbName(), $this->getDbPass(), array(PDO::ATTR_PERSISTENT => true));
            return $dbh;

        } catch (PDOException $exception) {
            $handler_errors = new Handler_errors();
            $handler_errors->set_PDO_error($exception->getMessage());
            die();
        }
    }

    /**
     * @param $table_name
     * @param $class_name_object
     * @return array
     */
    public function select_all($table_name, $class_name_object)
    {
        try {
            $dbh = $this->connection();
            $sql = "SELECT * FROM " . $table_name;
            $stmt = $dbh->query($sql);
            $stmt->setFetchMode(PDO::FETCH_CLASS, $class_name_object, array(0 => false));

            while ($row = $stmt->fetch()) {
                $array[] = $row;
            }
            $stmt->closeCursor();
            $dbh = null;
        } catch (PDOException $exception) {
            $handler_errors = new Handler_errors();
            $handler_errors->set_PDO_error($exception->getMessage());
            die();
        }
        if (empty($array)) {
            $array = array();
        }
        return $array;
    }

    /**
     * @param array $array
     * @param $table_name
     */
    public function insert(array $array, $table_name)
    {
        $parameters = '';
        $values = '';

        foreach ($array as $k => $v) {
            $parameters = $parameters . $k . ', ';
            $values = $values . ':' . $v . ', ';
        }
        $parameters = substr($parameters, 0, -2);
        $values = substr($values, 0, -2);

        $sql = "INSERT INTO " .
            $table_name .
            " (" .
            $parameters .
            ") values (" .
            $values .
            ")";

        try {
            $dbh = $this->connection();
            $stmt = $dbh->prepare($sql);
            $stmt->execute($array);
        } catch (PDOException $exception) {
            $handler_errors = new Handler_errors();
            $handler_errors->set_PDO_error($exception->getMessage());
            die();
        }
    }

    /**
     * @return mixed
     */
    public function getDbName()
    {
        return $this->db_name;
    }

    /**
     * @param mixed $db_name
     */
    public function setDbName($db_name)
    {
        $this->db_name = $db_name;
    }

    /**
     * @return mixed
     */
    public function getDbPass()
    {
        return $this->db_pass;
    }

    /**
     * @param mixed $db_pass
     */
    public function setDbPass($db_pass)
    {
        $this->db_pass = $db_pass;
    }

    /**
     * @return mixed
     */
    public function getHostName()
    {
        return $this->host_name;
    }

    /**
     * @param mixed $host_name
     */
    public function setHostName($host_name)
    {
        $this->host_name = $host_name;
    }

}