<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.02.2019
 * Time: 7:32
 */

class Mgr_recommendations
{
    private $recommendations;

    /**
     * Mgr_recommendations constructor.
     */
    public function __construct()
    {
        $this->setRecommendations($this->getRecommendations());
    }

    public function getData()//todo
    {
        $recommendations = '';
        foreach ($this->recommendations as $recommendation) {
            $recommendations = $recommendations . $recommendation->getName() . '&nbsp';
            $recommendations = $recommendations . $recommendation->getPosition() . '&nbsp';
            $recommendations = $recommendations . $recommendation->getCompany() . '&nbsp';
            $recommendations = $recommendations . $recommendation->getContacts() . '&nbsp';
            $recommendations = $recommendations . $recommendation->getText() . '&nbsp' . '</br>';
        }
        return $recommendations;
    }

    public function createRecommendation()//todo
    {
        $recommendation = array(
            'name' => 'name',
            'position' => 'position',
            'company' => 'company',
            'contacts' => 'contacts',
            'text' => 'text'
        );
        $pdo = new This_PDO();
        $pdo->insert($recommendation, 'recommendations');
    }

    /**
     * @return mixed
     */
    public function getRecommendations()
    {
        $pdo = new This_PDO();
        $recommendations = $pdo->select_all('recommendations', 'Recommendation');

        if (empty($recommendations)) {
            $this->createRecommendation();
            $recommendations = $pdo->select_all('recommendations', 'Recommendation');
        }
        $this->setRecommendations($recommendations);

        return $this->recommendations;
    }

    /**
     * @param mixed $recommendations
     */
    public function setRecommendations(array $recommendations)
    {
        $this->recommendations = $recommendations;
    }

}