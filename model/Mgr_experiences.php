<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.02.2019
 * Time: 7:33
 */

class Mgr_experiences
{

    private $experiences;

    /**
     * Mgr_experiences constructor.
     */
    public function __construct()
    {
        $this->setExperiences($this->getExperiences());
    }

    public function getData()//todo
    {


    }

    public function createExperience()//todo
    {
        $date = date("Y-m-d H:i:s");
        $experience = array(
            'company' => 'company',
            'date' => $date,
            'position' => 'position',
            'text' => 'text'
        );
        $pdo = new This_PDO();
        $pdo->insert($experience, 'experiences');
    }

    /**
     * @return mixed
     */
    public function getExperiences()
    {
        $pdo = new This_PDO();
        $experiences = $pdo->select_all('experiences', 'Experience');

        if (empty($experiences)) {
            $this->createExperience();
            $experiences = $pdo->select_all('experiences', 'Experience');
        }
        $this->setExperiences($experiences);

        return $this->experiences;
    }

    /**
     * @param mixed $experiences
     */
    public function setExperiences(array $experiences)
    {
        $this->experiences = $experiences;
    }

}