<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.04.2018
 * Time: 15:10
 */

class Persona
{
    private $login;
    private $pass;
    private $greeting;
    private $surname;
    private $name;
    private $patronymic;
    private $address;
    private $phones;
    private $fax;
    private $email;
    private $web_site;
    private $languages;
    private $profession;
    private $skills;
    private $field_of_activity;
    private $photo;
    private $driver_license;
    private $main_text;

    /**
     * Persona constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getGreeting()
    {
        return $this->greeting;
    }

    /**
     * @param mixed $greeting
     */
    public function setGreeting($greeting)
    {
        $this->greeting = $greeting;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param mixed $patronymic
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * @param mixed $phones
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getWebSite()
    {
        return $this->web_site;
    }

    /**
     * @param mixed $web_site
     */
    public function setWebSite($web_site)
    {
        $this->web_site = $web_site;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param mixed $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return mixed
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param mixed $profession
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param mixed $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @return mixed
     */
    public function getFieldOfActivity()
    {
        return $this->field_of_activity;
    }

    /**
     * @param mixed $field_of_activity
     */
    public function setFieldOfActivity($field_of_activity)
    {
        $this->field_of_activity = $field_of_activity;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getDriverLicense()
    {
        return $this->driver_license;
    }

    /**
     * @param mixed $driver_license
     */
    public function setDriverLicense($driver_license)
    {
        $this->driver_license = $driver_license;
    }

    /**
     * @return mixed
     */
    public function getMainText()
    {
        return $this->main_text;
    }

    /**
     * @param mixed $main_text
     */
    public function setMainText($main_text)
    {
        $this->main_text = $main_text;
    }

}