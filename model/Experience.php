<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.02.2019
 * Time: 14:18
 */

class Experience
{

    private $id;
    private $company;
    private $iddate;
    private $position;
    private $text;

    /**
     * Experience constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getIddate()
    {
        return $this->iddate;
    }

    /**
     * @param mixed $iddate
     */
    public function setIddate($iddate)
    {
        $this->iddate = $iddate;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

}