<?php

$http_path = HTTPPATH;

echo <<<HTML
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="$http_path/resources/js/jquery-3.1.1.js"></script>

    <link rel="stylesheet"
          href="$http_path/resources/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet"
          href="$http_path/resources/bootstrap/css/bootstrap-theme.css">

    <script src="$http_path/resources/bootstrap/js/bootstrap.js"></script>
       
    <link rel="stylesheet" href="$http_path/resources/css/style.css">
    
    <title>$title</title>
 
 <div align="center">header</div>
 
   <br/>  
   
</head>

HTML;
