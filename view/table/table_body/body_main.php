<?php

echo <<<HTML

<table width="99%" border="1" align="center"><!--  -->
    
    <tr align="center">
        <td rowspan="5">$photo</td>
        <td rowspan="5">$greeting</td>
        <td width="30%">$field_of_activity</td>
    </tr>

    <tr align="center">
        <td>$web_site</td>
    </tr>

    <tr align="center">
        <td>$phones</td>
    </tr>

    <tr align="center">
        <td>$email</td>
    </tr>

    <tr align="center">
        <td>$social_networks</td>
    </tr>

    <tr align="center">
        <td>$recommendations</td>
        <td>$skills</td>
        <td>$portfolios</td>
    </tr>

</table>

HTML;
