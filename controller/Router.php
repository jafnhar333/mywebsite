<?php


class Router
{
    private $request_uri;
    private $hand_request;

    public function __construct()
    {
        $this->setRequestUri(urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
    }

    /**
     *
     */
    public function visit_counter()//todo
    {
        //$arr_post = $this->getHandRequest()->getPost();
        //$arr_get = $this->getHandRequest()->getGet();
        //
        $this->check_url();
    }

    public function check_url()//todo более универсальный анализ пришедшего в адр. строке
    {
        $link = explode("/", $this->getRequestUri());
        $gener_view = new Generator_views();

        if (empty($link[1])) {
            $gener_view->create_main_page();
        } else {
            switch ($link[1]) {
                case "main":
                    $gener_view->create_main_page();
                    break;
                case "contacts":
                    $gener_view->create_contacts_page();
                    break;
                case "summary":
                    $gener_view->create_summary_page();
                    break;
                case "projects":
                    $gener_view->create_projects_page();
                    break;
                default:
                    include_once(ROOTPATH . '/404.php');
            }
        }
    }

    /**
     * @return string
     */
    public function getRequestUri()
    {
        return $this->request_uri;
    }

    /**
     * @param string $request_uri
     */
    public function setRequestUri($request_uri)
    {
        $this->request_uri = $request_uri;
    }

    /**
     * @return mixed
     */
    public function getHandRequest()
    {
        return $this->hand_request = new Handler_request();
    }

}