<?php


class Generator_tables
{
    private $table_header;
    private $table_body;
    private $table_footer;

    public function create_table_above()
    {
        $this->getTableHeader()->get_header();
        $this->getTableBody()->get_body_table_above();
        $this->getTableFooter()->get_footer();
    }

    public function create_table_lower()
    {
        $this->getTableHeader()->get_header();
        $this->getTableBody()->get_body_table_lower();
        $this->getTableFooter()->get_footer();
    }

    public function create_main_table()
    {
        $this->getTableHeader()->get_header();
        $this->getTableBody()->get_body_main();
        $this->getTableFooter()->get_footer();
    }

    public function create_summary_table()
    {
        $this->getTableHeader()->get_header();
        $this->getTableBody()->get_body_summary();
        $this->getTableFooter()->get_footer();
    }

    public function create_projects_table()
    {
        $this->getTableHeader()->get_header();
        $this->getTableBody()->get_body_projects();
        $this->getTableFooter()->get_footer();
    }
    public function create_contacts_table()
    {
        $this->getTableHeader()->get_header();
        $this->getTableBody()->get_body_contacts();
        $this->getTableFooter()->get_footer();
    }

    /**
     * @return Table_header
     */
    public function getTableHeader()
    {
        return $this->table_header = new Table_header();
    }

    /**
     * @return Table_body
     */
    public function getTableBody()
    {
        return $this->table_body = new Table_body();
    }

    /**
     * @return Table_footer
     */
    public function getTableFooter()
    {
        return $this->table_footer = new Table_footer();
    }

}