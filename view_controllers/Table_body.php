<?php

class Table_body
{
    private $mgr_persons;
    private $mgr_certificates;
    private $mgr_educations;
    private $mgr_experiences;
    private $mgr_portfolios;
    private $mgr_recommendations;
    private $mgr_social_networks;

    /**
     * Table_body constructor.
     */
    public function __construct()
    {
        $this->setMgrPersons($this->getMgrPersons());
        $this->setMgrCertificates($this->getMgrCertificates());
        $this->setMgrEducations($this->getMgrEducations());
        $this->setMgrExperiences($this->getMgrExperiences());
        $this->setMgrPortfolios($this->getMgrPortfolios());
        $this->setMgrRecommendations($this->getMgrRecommendations());
        $this->setMgrSocialNetworks($this->getMgrSocialNetworks());
    }

    /**
     *
     */
    public function get_body_table_above()
    {
        $name = $this->mgr_persons->getPersons()[0]->getName();
        $patronymic = $this->mgr_persons->getPersons()[0]->getPatronymic();
        $profession = $this->mgr_persons->getPersons()[0]->getProfession();
        include_once(ROOTPATH . '\view\table\table_body\body_table_above.php');
    }

    /**
     *
     */
    public function get_body_table_lower()
    {
        $name = $this->mgr_persons->getPersons()[0]->getName();
        $patronymic = $this->mgr_persons->getPersons()[0]->getPatronymic();
        $date = date("Y");
        $social_networks = '<div>' . $this->mgr_social_networks->getData() . '</div>';
        include_once(ROOTPATH . '\view\table\table_body\body_table_lower.php');
    }

    /**
     *
     */
    public function get_body_main()
    {
        $photo = $this->mgr_persons->getPersons()[0]->getPhoto();
        $greeting = $this->mgr_persons->getPersons()[0]->getgreeting();
        $field_of_activity = $this->mgr_persons->getPersons()[0]->getFieldOfActivity();
        $web_site = $this->mgr_persons->getPersons()[0]->getWebSite();
        $phones = $this->mgr_persons->getPersons()[0]->getPhones();
        $email = $this->mgr_persons->getPersons()[0]->getEmail();
        $skills = $this->mgr_persons->getPersons()[0]->getSkills();
        $social_networks = '<div>' . $this->mgr_social_networks->getData() . '</div>';
        $recommendations = '<div>' . $this->mgr_recommendations->getData() . '</div>';
        $portfolios = '<div>' . $this->mgr_portfolios->getData() . '</div>';
        include_once(ROOTPATH . '\view\table\table_body\body_main.php');
    }

    public function get_body_summary()
    {

    }

    public function get_body_projects()
    {

    }

    public function get_body_contacts()
    {

    }

    /**
     * @return mixed
     */
    public function getMgrPersons()
    {
        return $this->mgr_persons = new Mgr_persons();
    }

    /**
     * @param mixed $mgr_persons
     */
    public function setMgrPersons($mgr_persons)
    {
        $this->mgr_persons = $mgr_persons;
    }

    /**
     * @return mixed
     */
    public function getMgrCertificates()
    {
        return $this->mgr_certificates = new Mgr_certificates();
    }

    /**
     * @param mixed $mgr_certificates
     */
    public function setMgrCertificates($mgr_certificates)
    {
        $this->mgr_certificates = $mgr_certificates;
    }

    /**
     * @return mixed
     */
    public function getMgrEducations()
    {
        return $this->mgr_educations = new Mgr_educations();
    }

    /**
     * @param mixed $mgr_educations
     */
    public function setMgrEducations($mgr_educations)
    {
        $this->mgr_educations = $mgr_educations;
    }

    /**
     * @return mixed
     */
    public function getMgrExperiences()
    {
        return $this->mgr_experiences = new Mgr_experiences();
    }

    /**
     * @param mixed $mgr_experiences
     */
    public function setMgrExperiences($mgr_experiences)
    {
        $this->mgr_experiences = $mgr_experiences;
    }

    /**
     * @return mixed
     */
    public function getMgrPortfolios()
    {
        return $this->mgr_portfolios = new Mgr_portfolios();
    }

    /**
     * @param mixed $mgr_portfolios
     */
    public function setMgrPortfolios($mgr_portfolios)
    {
        $this->mgr_portfolios = $mgr_portfolios;
    }

    /**
     * @return mixed
     */
    public function getMgrRecommendations()
    {
        return $this->mgr_recommendations = new Mgr_recommendations();
    }

    /**
     * @param mixed $mgr_recommendations
     */
    public function setMgrRecommendations($mgr_recommendations)
    {
        $this->mgr_recommendations = $mgr_recommendations;
    }

    /**
     * @return mixed
     */
    public function getMgrSocialNetworks()
    {
        return $this->mgr_social_networks = new Mgr_social_networks();
    }

    /**
     * @param mixed $mgr_social_networks
     */
    public function setMgrSocialNetworks($mgr_social_networks)
    {
        $this->mgr_social_networks = $mgr_social_networks;
    }

}


