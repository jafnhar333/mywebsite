<?php

class Body
{
    public function start_body()
    {
        include_once(ROOTPATH . '/view/body/start_body.php');
    }

    public function end_body()
    {
        include_once(ROOTPATH . '/view/body/end_body.php');
    }

}
