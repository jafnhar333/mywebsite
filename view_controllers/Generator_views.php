<?php


class Generator_views
{
    private $header;
    private $body;
    private $footer;
    private $generator_tables;
    private $buttons_handler;

    public function create_main_page()
    {
        //phpinfo();
        $this->getHeader()->for_all('Главная', 'Главная страница');
        $this->getBody()->start_body();
        $this->getGeneratorTables()->create_table_above();
        $this->getGeneratorTables()->create_main_table();
        $this->getGeneratorTables()->create_table_lower();
        $this->getBody()->end_body();
        $this->getFooter();
    }

    public function create_summary_page()
    {
        $this->getHeader()->for_all('Резюме', 'Резюме');
        $this->getBody()->start_body();
        $this->getGeneratorTables()->create_table_above();
        $this->getGeneratorTables()->create_summary_table();
        $this->getGeneratorTables()->create_table_lower();
        $this->getBody()->end_body();
        $this->getFooter();
    }

    public function create_projects_page()
    {
        $this->getHeader()->for_all('Мои проекты', 'Проекты');
        $this->getBody()->start_body();
        $this->getGeneratorTables()->create_table_above();
        $this->getGeneratorTables()->create_projects_table();
        $this->getGeneratorTables()->create_table_lower();
        $this->getBody()->end_body();
        $this->getFooter();
    }

    public function create_contacts_page()
    {
        $this->getHeader()->for_all('Мои контакты', 'Контакты');
        $this->getBody()->start_body();
        $this->getGeneratorTables()->create_table_above();
        $this->getGeneratorTables()->create_contacts_table();
        $this->getGeneratorTables()->create_table_lower();
        $this->getBody()->end_body();
        $this->getFooter();
    }

    /**
     * @return Header
     */
    public function getHeader()
    {
        return $this->header = new Header();
    }

    /**
     * @return Body
     */
    public function getBody()
    {
        return $this->body = new Body();
    }

    /**
     * @return Footer
     */
    public function getFooter()
    {
        return $this->footer = new Footer();
    }

    /**
     * @return Generator_tables
     */
    public function getGeneratorTables()
    {
        return $this->generator_tables = new Generator_tables();
    }

    /**
     * @return Buttons_handler
     */
    public function getButtonsHandler()
    {
        return $this->buttons_handler = new Buttons_handler();
    }

}